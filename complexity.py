''' Vojtech Havlicek
    Self-Organised Critical Model
    Complexity project       2014
    Imperial College  '''

# Allow to change maximal recursion depth? 
# The code hits the limit for 64 even
import os             # This is useful for paths to files
import math
import random

from numpy import *    
from numpy.linalg import *
import matplotlib.pyplot as plt # Plotting engine

# List of system sizes to simulate 
#system_sizes = array([1,2,4,8,16,32,64]) # System sizes to be simulated

# Adjust a seed for the simulation
seed = 1005
random.seed(seed)

L              = 128     # System size. L = 1 is still ill behaved
G              = 3*L*(L+1) + 10000 # 2*128*128 # 2*L*(L+1)     # Number of grains to add ~ L^2 (time)
th = 5
probability    = 0.5       # Probability that a new slope treshold will be 1
pile_height    = 0         # Height
avalanche_size = 0         # Last sampled avalanche size
drop_size      = 0         # Last drop size

height_measurement    = zeros(G) # Time series of pile heights
avalanche_measurement = zeros(G) # Avalanche time series
dropsize_measurement  = zeros(G) # Drop size measurement

slopes    = zeros(L) # Create a new empty list of system size with slopes
tresholds = zeros(L) # A list of tresholds

# How to construct the zero-filled L-length list without need of
# doing the following ?
# 1. Initialize
for k in range(0,L):
    tresholds[k] = random.randint(1,th)
    #  tresholds[k] = 1 # BTW model

def generate_treshold(position, probability) :
    ''' Generates a new treshold value z_th from {1,2} at random weighted by 
        the probability. There is a probability "probability" that the result 
        will be 1 '''
    global th;
    return random.randint(1, th)
    #if random.random() < probability :
     #    return 1
    #else :
     #    return 2

def relax_nonrecursive(position):
  ''' Nonrecursive implementation of the above to avoid Python's lack of
  tail recursion optimisation preventing to use system sizes bigger than 
  L = 32 '''
  
  global probability
  global pile_height
  global avalanche_size
  global drop_size

  relax_list = []
  relax_list.append(position)

  while len(relax_list) > 0 :
    pos = relax_list.pop(0)
 
    if L == 1 :  # The algorithm behaves a bit differently for L = 1 
      if slopes[pos] > tresholds[pos] :
        pile_height -= 1                                           
        avalanche_size += 1                                        
        drop_size += 1                                             
        slopes[0] -= 1                                             
        tresholds[0] = generate_treshold(0, probability)           
        relax_list.append(0) # It may happen that two drops are induced

    else: # Normal behaviour
      if slopes[pos] > tresholds[pos] :
         avalanche_size += 1
      
         if pos == 0:
            slopes[0] = slopes[0] - 2
            slopes[1] = slopes[1] + 1
            tresholds[pos] = generate_treshold(pos, probability)
            relax_list.append(1)  # Is particular ordering needed?
            relax_list.append(0)
       
            pile_height -= 1
         elif pos == L-1 :      # A grain drops off the system 
            slopes[L-1] = slopes[L-1] - 1
            slopes[L-2] = slopes[L-2] + 1
            tresholds[L-1] = generate_treshold(pos, probability)
            relax_list.append(L-2)
            relax_list.append(L-1) # Append the final site as well - we
                                   # decreased by 1 only!
            
            drop_size += 1
         else:
            slopes[pos] -= 2
            slopes[pos+1] += 1
            slopes[pos-1] += 1
            tresholds[pos] = generate_treshold(pos, probability)
            relax_list.append(pos-1)
            relax_list.append(pos)
            relax_list.append(pos+1)

# Iterate the algorithm. Can I just use for instead of while loop?
for counter in range(0,G):
    slopes[0]   += 1 # 2. Drive. Add a grain at position 0 
    pile_height += 1 # Grain was added to i = 1. Increase the height

    # 3. Relax. 
    # The routine also updates the avalanche size, which is added
    # in time series
    drop_size = 0
    avalanche_size = 0
    relax_nonrecursive(0) # relax(0) # Stop hitting recursion depth limit here ...

    if drop_size > 0 :
       dropsize_measurement[counter] = drop_size
       
       #if recurrent == False :
       #    print "Recurrent configurations: ",counter
       #    recurrent = True

    #drop_size = 0
    
    avalanche_measurement[counter] = avalanche_size

    # 3.5. Obtain data
    # check_height = sum(slopes)
    # print check_height, pile_height
    height_measurement[counter] = pile_height

    # 4. Iterate again

print "2.a All done, write the results to files & show the graphs!"

# Prints out the height measurement - how to read it back, plot and fit?
f = file("height_measurement_"+str(L)+'_'+str(seed), 'w')
for entry in height_measurement : 
    f.write(str(entry) + '\n')
f.close()

# Task 2.b
###################################################################
print "Starting Task 2.b measurement"
W = 50;  # L
# Why not use dynamic window of system size? 

moving_average_data = []

for n in range(0,len(height_measurement)) :
    lower =  int(n - W*.5)
    if n - W*.5 < 0 :
       lower = 0

    upper = int(n + W*.5) 
    if upper > len(height_measurement) :
       upper = len(height_measurement)
    
    window = upper-lower
    
    moving_average_data.append(float(sum(height_measurement[lower:upper]))/float(window))

# Get rid of edge effects
if len(moving_average_data) > W:
  print "Ok, enough data collected"
  moving_average_data = moving_average_data[int(W/2) : len(moving_average_data)-int(W/2)]
else:
    moving_average_data = None
    print "insufficient amount of data collected"
    exit()

# Write the moving average to the file
f = file("height_measurement_moving_average_"+str(L)+"_"+str(seed), 'w')
for entry in moving_average_data : 
    f.write(str(entry) + '\n')
f.close()

# Scale the height of MA
moving_averages_scaled = [] 
k = 0 
while k < len(moving_average_data) :
   moving_averages_scaled.append( float(moving_average_data[k])/float(L))
   k += 1 

f = file("height_measurement_moving_average_scaled_"+str(L)+'_'+str(seed), 'w') 
k = float(W*.5)
for entry in moving_average_data :                         
    k += 1.0
    f.write(str(float(entry)/math.sqrt(float(k))) + '\t' + str(float(k)/float(L*L)) + '\n')     
f.close()

exit();
# Task 3.a
######################################################################
print("Starting task 3.a measurement")
begin = L*(L+1) # Index at which it is guaranteed that the system entered the recurent configurations 
end =   G # Total number of samples

# Prepare the height data from recurrent configuration
steady_state_data = height_measurement[begin: end] 

height_sqrd = []
for measurement in steady_state_data :
    height_sqrd.append(measurement*measurement)

if len(steady_state_data) != end-begin : # Print out an error if dataset is too small
  print ("Error - not enough steady state data. Adjust G")
  exit()

average_height         = float(sum(steady_state_data))/float(end-begin) # First moment
average_height_squared = float(sum(height_sqrd))/float(end-begin) # Second moment

# Compute the standard deviation
standard_deviation = math.sqrt(average_height_squared - average_height**2)
print "Std. deviation : ", standard_deviation 

# Prepare the probability distribution
height_probability = dict()       # Probability distribution of heights
for height in steady_state_data : # Fill it in
    if height_probability.has_key(height) == False :
       height_probability[height] = 1 
    else :
       height_probability[height] += 1 

for key in height_probability.iterkeys() : # Normalize
    height_probability[key] = float(height_probability[key])/float(end-begin)

def P_height(height) : # Define the height P function for convenience 
    if height_probability.has_key(height) == False :
      return 0
    else :
      return height_probability[height]

print height_probability.keys()

# Plot the PDF 
height_probabilities = [0]*(2*L + 1) # max height. Range from 0 to 2L + 1
for entry in range(0, 2*L + 1) :
    height_probabilities[entry] = P_height(entry)

print "Probability normalisation check: ", sum(height_probabilities)

f = file("average_height_"+str(L)+'_'+str(seed), "w")
f.write(str(L) + '\t' + str(average_height) + '\t' + 
          str(standard_deviation) + '\t' + 
          str(end-begin))
f.close()

f = file("height_probability_"+str(L) +'_'+str(seed), "w")
for index in range(0, len(height_probabilities)):
    f.write(str(index) + '\t' + str(height_probabilities[index])+'\n')
f.close()

# Plot height probability distribution                                                   
#plt.figure()                                                                             
#plt.title("$P(h,L)$")                                                                    
#plt.plot(height_probabilities, 'r-')                                                     
#plt.xlabel("height")                                                                     
#plt.xlabel("probability")                                                                
#plt.show()

# Task 4.a
##################################################
print("Starting task 4 measurements")
steady_state_avalanche_data = avalanche_measurement[begin:end]
max_avalanche = max(steady_state_avalanche_data)

avalanche_size_probability = zeros(max_avalanche + 1) # Av. size ltd by system size  

for size in steady_state_avalanche_data : # Fill it in       
    avalanche_size_probability[size] += 1                

avalanche_size_probability = avalanche_size_probability/float(end-begin) 
#print sum(avalanche_size_probability) # Check normalisation

f = file("avalanche_size_probability_"+str(L)+"_"+str(end-begin)+'_'+str(seed), "w")                            
for index in range(0, int(max_avalanche+1)):                      
    f.write(str(index) + '\t' + str(avalanche_size_probability[index])+'\n') 
f.close()                                                              

# Task 4.d - moment analysis
###################################################
moment_powers = array([1,2,3,4,5,6])
moments = []

for power in moment_powers:
  moments.append(sum(steady_state_avalanche_data**power)/float(end-begin))

moment_data = zip(moment_powers, moments) # How come the 1st moment scales ss?

f = file("avalanche_moments_"+str(L)+"_"+str(end-begin) + "_" + str(seed), "w")
for k, moment in moment_data:
  f.write(str(k) + '\t' +  str(moment) +'\n')
f.close()

# Task 5 - dropsize measurement
#########################################
print("Starting Task 5 - drop size measurement")
steady_state_dropsize = dropsize_measurement[begin:end]
max_dropsize = max(steady_state_dropsize)

drop_size_probability = zeros(max_dropsize+1)

for size in steady_state_dropsize : 
  drop_size_probability[size] += 1

# avoid 0 size drops
drop_size_probability = drop_size_probability/float(end - begin - drop_size_probability[0])
drop_size_probability = drop_size_probability[1:]

plt.figure()
plt.plot(drop_size_probability, 'bo')
#plt.xscale('log')
#plt.yscale('log')


f = file("drop_size_probability_"+str(L)+"_"+str(end-begin)+'_'+str(seed), "w") 
for index in xrange(1, int(max_dropsize)):
  f.write(str(index) + '\t' +  str(drop_size_probability[index]) +'\n')                                   
f.close()                                                                       

plt.show()

print sum(drop_size_probability)


# Plotting
######################################

# Plot height moving average data:
#plt.figure()                                                                            
##plt.plot(range(int(W/2),len(moving_average_data)+int(W/2)), moving_average_data, 'ro') 
#plt.plot( moving_average_data, 'r-')                                                    
#                                                                                        
## Plot Avalanche size distribution
#plt.figure()
#plt.xscale('log')
#plt.yscale('log')
#plt.title("$P_N(s,L)$")
#plt.xlabel("s")
#plt.ylabel("probability")
#plt.plot(avalanche_probabilities, 'r-')
#
#plt.show();


